//
//  AvatarView.swift
//  Notch Animation
//
//  Created by Lucas Gomes on 01/06/23.
//

import SwiftUI

struct AvatarView: View {
    
    @State private var currentImageIndex = 1
    let timer = Timer.publish(every: 3, on: .main, in: .common).autoconnect()

    var body: some View {
        Image("avatar-\(currentImageIndex)")
            .resizable()
            .scaledToFit()
            .onReceive(timer) { _ in
                withAnimation {
                    currentImageIndex = (currentImageIndex % 4) + 1
                }
            }
    }
}
