//
//  Notch_AnimationApp.swift
//  Notch Animation
//
//  Created by Lucas Gomes on 01/06/23.
//

import SwiftUI

@main
struct Notch_AnimationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
