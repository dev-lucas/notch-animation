//
//  Home.swift
//  Notch Animation
//
//  Created by Lucas Gomes on 01/06/23.
//

import SwiftUI

struct Home: View {
    
    /*
     Dynamic Island -> Safe Area top = above 50
     Notch -> Safe Area top = between 45 and 47
     No Notch -> Safe Area bottom = 0
     */
    
    var size: CGSize
    var safeArea: EdgeInsets
    @State private var scrollProgress: CGFloat = 0
    @Environment(\.colorScheme) private var colorScheme
    @State private var textHeaderOffset: CGFloat = 0
    private let coordinateSpaceName = "ScrollView"
    private let anchorPreferenceName = "Header"
    
    var body: some View {
        let isHavingNotch = safeArea.bottom != 0
        let isHavingDynamicIsland = safeArea.top > 51
        let capsuleHeight = isHavingDynamicIsland ? 37 : safeArea.top - 15
        let safeAreaTop: CGFloat = safeArea.top + 3
        
        ScrollView(.vertical, showsIndicators: true) {
            VStack(spacing: 12) {
                AvatarView()
                    .frame(
                        width: 130 - (75 * scrollProgress),
                        height: 130 - (75 * scrollProgress)
                    )
                    .opacity(1 - scrollProgress)
                    .blur(radius: scrollProgress * 10, opaque: true)
                    .clipShape(Circle())
                    .anchorPreference(key: AnchorKey.self, value: .bounds, transform: {
                        return [anchorPreferenceName: $0]
                    })
                    .padding(.top, safeArea.top + 15)
                    .offsetExtractor(coordinateSpace: coordinateSpaceName) { scrollRect in
                        guard isHavingNotch else { return }
                        let progress = -scrollRect.minY / 25
                        scrollProgress = min(max(progress, 0), 1)
                    }
                
                Text("amrit.art/toy-faces")
                    .font(.caption)
                    .foregroundColor(.gray)
                    .padding(.vertical, 15)
                    .background(
                        Rectangle()
                            .fill(colorScheme == .dark ? .black : .white)
                            .frame(width: size.width)
                            .padding(.top, textHeaderOffset < safeAreaTop ? -safeAreaTop : 0)
                            .shadow(color: .black.opacity(textHeaderOffset < safeAreaTop ? 0.1 : 0), radius: 5, x: 0, y: 5)

                    )
                    .offset(y: textHeaderOffset < safeAreaTop ? -(textHeaderOffset - safeAreaTop) : 0)
                    .offsetExtractor(coordinateSpace: coordinateSpaceName) {
                        textHeaderOffset = $0.minY
                    }
                    .zIndex(1)
                
                SkeletonRow()
            }
            .frame(maxWidth: .infinity)
        }
        .backgroundPreferenceValue(AnchorKey.self, { pref in
            GeometryReader { proxy in
                if let anchor = pref[anchorPreferenceName], isHavingNotch {
                    let frameRect = proxy[anchor]
                    
                    Canvas { out, size in
                        out.addFilter(.alphaThreshold(min: 0.5))
                        out.addFilter(.blur(radius: 12))
                        
                        out.drawLayer { ctx in
                            if let headerView = out.resolveSymbol(id: 0) {
                                ctx.draw(headerView, in: frameRect)
                            }
                            
                            if let dynamicIsland = out.resolveSymbol(id: 1) {
                                let rect = CGRect(
                                    x: (size.width - 120) / 2, /* 120 -> capsule width */
                                    y: isHavingDynamicIsland ? 11 : 0, /* 11 -> max for dynamic island */
                                    width: 120,
                                    height: capsuleHeight
                                )
                                ctx.draw(dynamicIsland, in: rect)
                            }
                        }
                    } symbols: {
                        HeaderView(frameRect)
                            .tag(0)
                            .id(0)
                        
                        DynammicIslandCapsule()
                            .tag(1)
                            .id(1)
                    }
                }
            }
            .overlay(alignment: .top) {
                Rectangle()// For not overcoming dynamic island
                    .fill(colorScheme == .dark ? .black : .white)
                    .frame(height: 15)
            }
        })
        .overlay(alignment: .top, content: {
            HStack {
                Button {} label: {
                    Label("Back", systemImage: "chevron.left")
                }
                
                Spacer()
                
                Button {} label: {
                    
                    Text("Edit")
                }
            }
            .padding(15)
            .padding(.top, safeAreaTop - 3)
        })
        .coordinateSpace(name: coordinateSpaceName)
    }
    
    @ViewBuilder
    func HeaderView(_ frameRect: CGRect) -> some View {
        Circle()
            .fill(.black)
            .frame(width: frameRect.width, height: frameRect.height)
    }
    
    @ViewBuilder
    func DynammicIslandCapsule(_ height: CGFloat = 37) -> some View {
        Capsule()
            .fill(.black)
            .frame(width: 120, height: height)
    }
    
    @ViewBuilder
    func SkeletonRow() -> some View {
        VStack {
            ForEach(0..<20, id: \.self) { _ in
                VStack(alignment: .leading, spacing: 6) {
                    RoundedRectangle(cornerRadius: 5, style: .continuous)
                        .fill(.gray.opacity(0.15))
                        .frame(height: 25)
                    
                    RoundedRectangle(cornerRadius: 5, style: .continuous)
                        .fill(.gray.opacity(0.15))
                        .frame(height: 15)
                        .padding(.trailing, 50)
                    
                    RoundedRectangle(cornerRadius: 5, style: .continuous)
                        .fill(.gray.opacity(0.15))
                        .frame(height: 15)
                        .padding(.trailing, 150)
                }
            }
        }
        .padding(.horizontal, 15)
        .padding(.bottom, safeArea.bottom + 15)
    }
    
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
